package datastruct

// AuthorAuthorLink ...
type AuthorAuthorLink struct {
	ID       int64
	BookID   int64
	AuthorID int64
}
